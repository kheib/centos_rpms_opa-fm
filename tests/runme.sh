#!/bin/bash
set -x

# https://projects.engineering.redhat.com/browse/PLATFORMCI-2494

# This is a workaround for opa-fm OSCI gating test. Until OSCI
# team resolves the ticket PLATFORMCI-2494, we can't run opa-fm
# beaker case over rdma-qe-14/15.

exit 0
